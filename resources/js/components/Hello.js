import React from 'react'
import { createRoot } from 'react-dom/client';

export default function Hello () {
    return (
        <h1>Hello React!</h1>
    )
}

window.addEventListener('load', function(event) {
    console.log("LOADED!!!!");
    const container = document.getElementById('root');
    const root = createRoot(container);
    root.render(<Hello />);
})